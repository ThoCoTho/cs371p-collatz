# CS371p: Object-Oriented Programming Collatz Repo

* Name: Thomas Connor Thompson

* EID: tct572

* GitLab ID: ThoCoTho

* HackerRank ID: thocotho

* Git SHA: 8c34b0c902388c4ed72932223461e71674455b29

* GitLab Pipelines: https://gitlab.com/ThoCoTho/cs371p-collatz/-/pipelines/189431884

* Estimated completion time: 5

* Actual completion time: 7

* Comments: I really enjoyed having all these little tools that told me a lot more about my code
