var searchData=
[
  ['collatz_5feval',['collatz_eval',['../Collatz_8cpp.html#a1fd62f9e2b2d968d8fb3a0dcd218fc2c',1,'collatz_eval(const pair&lt; int, int &gt; &amp;p):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a52d2fa1b09ed6ed338cb7931ae376986',1,'collatz_eval(const pair&lt; int, int &gt; &amp;):&#160;Collatz.cpp']]],
  ['collatz_5fprint',['collatz_print',['../Collatz_8cpp.html#ab6092110b1525ab0de679ca0b20e13eb',1,'collatz_print(ostream &amp;sout, const tuple&lt; int, int, int &gt; &amp;t):&#160;Collatz.cpp'],['../Collatz_8hpp.html#abc6faa0ba3de27b9eb9b762dedf41f83',1,'collatz_print(ostream &amp;, const tuple&lt; int, int, int &gt; &amp;):&#160;Collatz.cpp']]],
  ['collatz_5fread',['collatz_read',['../Collatz_8cpp.html#a2772f8a734aeab48332eb3b282f991ba',1,'collatz_read(const string &amp;s):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a030060205cce5a0fef7c6bef2d915c9f',1,'collatz_read(const string &amp;):&#160;Collatz.cpp']]],
  ['collatz_5fsolve',['collatz_solve',['../Collatz_8cpp.html#ab43d648b4711e8c591a9739f196f1312',1,'collatz_solve(istream &amp;sin, ostream &amp;sout):&#160;Collatz.cpp'],['../Collatz_8hpp.html#ac8d784fbd05e60bbd57a4b2db5ba6b87',1,'collatz_solve(istream &amp;, ostream &amp;):&#160;Collatz.cpp']]],
  ['compute_5fcycle',['compute_cycle',['../Collatz_8cpp.html#a118677cfc0f6191599de7404bfb58bc3',1,'compute_cycle(int input):&#160;Collatz.cpp'],['../Collatz_8hpp.html#abe8e79e2bccdec240fcd7d490b47c2b0',1,'compute_cycle(int):&#160;Collatz.cpp']]]
];
