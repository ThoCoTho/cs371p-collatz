// -----------
// Collatz.c++
// -----------
//Testing
// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

int cycleArr[1000000] = {0};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i;
    int j;
    int cycle = 0;

    if (p.first > p.second)
        tie(j, i) = p;
    else
        tie(i, j) = p;

    assert(i <= j);
    int med = j / 2 + 1; //By the nature of the Collatz cycle we can reduce the range of numbers to search
    if (i < med)
        i = med;
    for (; i <= j; i++) {
        int toCheck = get_cycle(i);
        if (toCheck > cycle)
            cycle = toCheck;
    }
    return make_tuple(p.first, p.second, cycle);
}

// -------------
// get_cycle
// -------------

int get_cycle(int input) {
    if (cycleArr[input] == 0) {
        int cycle = compute_cycle(input);
        cycleArr[input] = cycle;
    }
    return cycleArr[input];
}

// -------------
// compute_cycle
// -------------

int compute_cycle(int input) {
    long i = input;
    int cycle = 1;
    assert(i > 0);
    while (i > 1) {
        if (i % 2 == 0) {
            i = i>>1;
            cycle++;
        }
        else {
            i = i + (i >> 1) + 1;
            cycle+=2;
        }
    }
    assert(cycle >= 0);
    return cycle;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
